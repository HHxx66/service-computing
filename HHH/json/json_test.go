package json

import "fmt"

type Monitor struct {
	ID		int
	Name		string
}

type Group struct {
	ID		int
	Name		string
	Members	[]string
	Nums		[3]int
	M		Monitor			`mytag:"GG"`
	Manage	map[int][]int
}

func ExampleSliceMarshal() {
	b, err := sliceMarshal([]int{1,2,3})
	if err != nil {
	    fmt.Println("error:", err)
	}
	fmt.Println(string(b))
	//Output:[1,2,3]
}

func ExampleStructMarshal() {
	group := Group{
		ID:1,
		Name:"Reds",
		Members:[]string{"Crimson", "Red", "Ruby", "Maroon"},
		Nums:[3]int{1,2,3},
		M:Monitor{18666,"yzdl"},
	}
	group.Manage= make(map[int][]int)
	group.Manage[2] = []int{1,2,3}
	b, err := structMarshal(group)
	if err != nil {
	    fmt.Println("error:", err)
	}
	fmt.Println(string(b))
	//Output:{"ID":1,"Name":"Reds","Members":["Crimson","Red","Ruby","Maroon"],"Nums":[1,2,3],"GG":{"ID":18666,"Name":"yzdl"},"Manage":{"2":[1,2,3]}}
}

func ExampleMapMarshal() {
	M := make(map[int][]int)
	M[2] = []int{1,2,3}
	b, err := mapMarshal(M)
	if err != nil {
	    fmt.Println("error:", err)
	}
	fmt.Println(string(b))
	//Output:{"2":[1,2,3]}
}

func ExampleMarshal() {
	b, err := marshal(123)
	if err != nil {
	    fmt.Println("error:", err)
	}
	fmt.Println(string(b))
	//Output:123
}

func ExampleJsonMarshal() {
	group := Group{
		ID:1,
		Name:"Reds",
		Members:[]string{"Crimson", "Red", "Ruby", "Maroon"},
		Nums:[3]int{1,2,3},
		M:Monitor{18666,"yzdl"},
	}
	group.Manage= make(map[int][]int)
	group.Manage[2] = []int{1,2,3}
	b, err := JsonMarshal(&group)
	if err != nil {
	    fmt.Println("error:", err)
	}
	fmt.Println(string(b))
	//Output:{"ID":1,"Name":"Reds","Members":["Crimson","Red","Ruby","Maroon"],"Nums":[1,2,3],"GG":{"ID":18666,"Name":"yzdl"},"Manage":{"2":[1,2,3]}}
}
