package main

import (
	"fmt"
	"github.com/github-user/HHH/json"
)

type Group struct {
	ID		int
	Name		string
}


type ColorGroup struct {
	ID		int
	Name		string
	Colors	[]string
	Nums		[3]int
	G		Group			`mytag:"GG"`
	M		map[int][]int
}

func main() {
	group := ColorGroup{
		ID:1,
		Name:"Reds",
		Colors:[]string{"Crimson", "Red", "Ruby", "Maroon"},
		Nums:[3]int{1,2,3},
		G:Group{1,"123"},
	}
	group.M  = make(map[int][]int)
	group.M[2] = []int{1,2,3}
	b, err := json.JsonMarshal(group)
	if err != nil {
	    fmt.Println("error:", err)
	}
	fmt.Println(string(b))
	b, err = json.JsonMarshal(&group)
	if err != nil {
	    fmt.Println("error:", err)
	}
	fmt.Println(string(b))
	b, err = json.JsonMarshal("123456")
	if err != nil {
	    fmt.Println("error:", err)
	}
	fmt.Println(string(b))
}
