package main
import(
	"bufio"
	"io"
	"os"
)
func main(){
	reader:=bufio.NewReader(os.Stdin)
	file,openErr:=os.OpenFile("./lp1.txt",os.O_RDWR|os.O_CREATE|os.O_APPEND|os.O_TRUNC,0644)
	if openErr!=nil {
		panic(openErr)
	}
	writer:=bufio.NewWriter(file)
	for{
		line,errRead:=reader.ReadBytes('\n')
		if errRead!=nil {
			if errRead==io.EOF {
				break
			} else {
				os.Stderr.Write([]byte("Read bytes from reader fail\n"))
				os.Exit(0)
			}
		}
		_,errWrite:=writer.Write(line)
		if errWrite!=nil {
			os.Stderr.Write([]byte("Write bytes to file fail\n"))
			os.Exit(0)
		}
		writer.Flush()
	}
}
