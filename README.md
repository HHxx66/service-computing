# go开发简单selpg程序的代码使用方式

首先在虚拟机配置好go语言开发环境，具体可参照我的第一次作业[服务计算(1)——安装 go 语言开发环境](https://blog.csdn.net/qq_43278234/article/details/108566781)

## 安装golang

``sudo yum install golang --nogpgcheck``

## 设置环境变量：

通过vi编辑~/.profile文件，这个文件不存在（即显示为新文件）也没有关系。在文件中添加：

``export GOPATH=$HOME/gowork``

``export PATH=$PATH:$GOPATH/bin``

最后执行这些配置

``source $HOME/.profile``

## 程序运行

将代码文件放入路径$GOPATH/src/github.com/github-user/。

安装spf13/pflag

``go get github.com/spf13/pflag``

最后执行``go install github.com/github-user/lp1``和``go install github.com/github-user/selpg``两条指令即可。

1. ``selpg -s1 -e1 input_file``

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006215329611.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006215352645.png#pic_center)

2.``selpg -s1 -e1 < input_file``

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006215759523.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

这里同样也是72行为一页，但在这里，最后一个参数并不是作为selpg的源输入文件参数，当前selpg读取的是标准输入，只不过是这时候标准输入已被shell／内核重定向为来自“input_file”而不是显式命名的文件名参数。

这是shell自行实现了的，因而并不在我们程序的实现范围之内。

3.``other_command | selpg -s10 -e20``

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006220239516.png#pic_center)

这个也是shell自行实现好了的，“other_command”的标准输出被shell／内核重定向至 selpg 的标准输入。因为总共也不够72行(总共才1页)，因而要输出第10到第20页的话就是空值。

4.``selpg -s10 -e20 input_file >output_file``

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006220553123.png#pic_center)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006220652510.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

因为我的in.txt总共才101行，把第1页到第3页输出到out.txt的话，实际上也就输出了两页。这里同样地，shell也已经帮我们实现好了输出的重定向，标准输出被shell／内核重定向至“output_file”。

5.``selpg -s10 -e20 input_file 2>error_file``

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006221706799.png#pic_center)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006221734910.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

输入一个错误的指令，就能看到错误信息输出到err.txt里面了。所有的错误消息被shell／内核重定向至“error_file”。请注意：在“2”和“>”之间不能有空格；这是 shell 语法的一部分（请参阅“man bash”或“man sh”）。

6.``selpg -s10 -e20 input_file >output_file 2>error_file``

![在这里插入图片描述](https://img-blog.csdnimg.cn/202010062220533.png#pic_center)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006222110469.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006222126919.png#pic_center)

out.txt里面新得到的内容跟先前的测试一样，同时err.txt里面的内容被清空了。

7.``selpg -s10 -e20 input_file >output_file 2>/dev/null``

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006222537219.png#pic_center)

写至标准输出的内容被重定向到了out.txt，写至标准错误的内容被丢弃。

8.``selpg -s10 -e20 input_file >/dev/null``

![在这里插入图片描述](https://img-blog.csdnimg.cn/2020100622284667.png#pic_center)

9.``selpg -s10 -e20 input_file | other_command``

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006223003927.png#pic_center)

同时，这里的other_command还可以设置为先前自行写的模拟lp1程序。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006223144838.png#pic_center)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006223200601.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

10.``selpg -s10 -e20 input_file 2>error_file | other_command``

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006223246641.png#pic_center)

1.``selpg -s10 -e20 -l66 input_file``

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006223351608.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006223405883.png#pic_center)

2.``selpg -s10 -e20 -f input_file``

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006223501113.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006223514842.png#pic_center)

(这里，我在第72行的后面加了一个'\f'。)

3.``selpg -s10 -e20 -dlp1 input_file``

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006223641528.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006223703317.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

4.``selpg -s10 -e20 input_file > output_file 2>error_file &``

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201006224040659.png#pic_center)

~~由于打印的内容太少，一下子就执行完了。~~

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201008114550599.png#pic_center)