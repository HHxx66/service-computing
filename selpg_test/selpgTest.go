package main

import (
	"fmt"
	"os"
	"os/exec"
)

func main(){
	var N,count int
	fileIn,err:=os.Open("in.txt")
	fileOut,err:=os.Open("out.txt")
	if err!=nil {
		fmt.Print(err)
		fmt.Print('\n')
	}
	dataIn:=make([]byte, 1000)
	dataOut:=make([]byte, 1000)
	N,err=fileIn.Read(dataIn)
	if err!=nil {
		fmt.Print(err)
		fmt.Print('\n')
	}
	cmd:=exec.Command("bash","-c","selpg -s1 -e1 in.txt >out.txt")
	if err = cmd.Run(); err != nil {
		fmt.Print(err)
		fmt.Print('\n')
	}
	count,err=fileOut.Read(dataOut)
	if err!=nil {
		fmt.Print(err)
		fmt.Print('\n')
	}
	count=0
	for i:=0;count<72;i++ {
		if dataIn[i]!=dataOut[i] {
			fmt.Println("Failed at test1.")
			os.Exit(0)
		}
		if dataIn[i]=='\n' {
			count++;
		}
	}
	cmd=exec.Command("bash","-c","selpg -s1 -e1 -f in.txt >out.txt")
	if err = cmd.Run(); err != nil {
		fmt.Print(err)
		fmt.Print('\n')
	}
	fileOut,err=os.Open("out.txt")
	if err!=nil {
		fmt.Print(err)
		fmt.Print('\n')
	}
	count,err=fileOut.Read(dataOut)
	if err!=nil {
		fmt.Print(err)
		fmt.Print('\n')
	}
	count=0
	for i:=1;count<1;i++ {
		if dataIn[i]!=dataOut[i] {
			fmt.Println("Failed at test2.")
		}
		if dataIn[i]=='\f' {
			count++;
		}
	}
	cmd=exec.Command("bash","-c","selpg -s2 -e2 -l66 in.txt >out.txt")
	if err = cmd.Run(); err != nil {
		fmt.Print(err)
		fmt.Print('\n')
	}
	fileOut,err=os.Open("out.txt")
	if err!=nil {
		fmt.Print(err)
		fmt.Print('\n')
	}
	count,err=fileOut.Read(dataOut)
	if err!=nil {
		fmt.Print(err)
		fmt.Print('\n')
	}
	count=0
	i:=0
	for ;count<66;i++ {
		if dataIn[i]=='\n' {
			count++;
		}
	}
	count=0
	for j:=0;count<66&&i<N; {
		if dataIn[i]!=dataOut[j] {
			fmt.Println("Failed at test3.")
			os.Exit(0)
		}
		if dataIn[i]=='\n' {
			count++;
		}
		i++;
		j++
	}
	cmd=exec.Command("bash","-c","selpg -s2 -e2 -l66 -dlp1 in.txt >out.txt")
	if err = cmd.Run(); err != nil {
		fmt.Print(err)
		fmt.Print('\n')
	}
	fileOut,err=os.Open("lp1.txt")
	if err!=nil {
		fmt.Print(err)
		fmt.Print('\n')
	}
	count,err=fileOut.Read(dataOut)
	if err!=nil {
		fmt.Print(err)
		fmt.Print('\n')
	}
	count=0
	for i=0;count<66;i++ {
		if dataIn[i]=='\n' {
			count++;
		}
	}
	count=0
	for j:=0;count<66&&i<N; {
		if dataIn[i]!=dataOut[j] {
			fmt.Printf("%d %d %q %q\n",i,j,dataIn[i],dataOut[j])
			fmt.Println("Failed at test4.")
			os.Exit(0)
		}
		if dataIn[i]=='\n' {
			count++;
		}
		i++;
		j++
	}
	fmt.Println("PASS.")
}
