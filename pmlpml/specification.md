# filtering操作的实现

首先，[filtering](http://reactivex.io/documentation/operators.html#filtering) 有如下功能：

 - **Debounce** — only emit an item from an Observable if a particular timespan has passed without it emitting another item
 - **Distinct** — suppress duplicate items emitted by an Observable
 - **ElementAt** — emit only item n emitted by an Observable
 - **Filter** — emit only those items from an Observable that pass a predicate test
 - **First** — emit only the first item, or the first item that meets a condition, from an Observable
 - **IgnoreElements** — do not emit any items from an Observable but mirror its termination notification
 - **Last** — emit only the last item emitted by an Observable
 - **Sample** — emit the most recent item emitted by an Observable within periodic time intervals
 - **Skip** — suppress the first n items emitted by an Observable
 - **SkipLast** — suppress the last n items emitted by an Observable
 - **Take** — emit only the first n items emitted by an Observable
 - **TakeLast** — emit only the last n items emitted by an Observable

其中**Filter**这一功能老师在``transforms.go``里面已经实现了，因此在这里我们也不需要再去实现。

首先，结构体**filteringOperator**可以直接从``transforms.go``里照搬过去，改改名字。

```go
type filteringOperator struct {
	opFunc func(ctx context.Context, o *Observable, item reflect.Value, out chan interface{}) (end bool)
}
```

op大抵也是照搬过去的，只是有些许改变：

```go
func (ftop filteringOperator) op(ctx context.Context, o *Observable) {
	in := o.pred.outflow
	out := o.outflow
	//fmt.Println(o.name, "operator in/out chan ", in, out)

	go func() {
		end := false
		for x := range in {
			if end {
				break
			}
			// can not pass a interface as parameter (pointer) to gorountion for it may change its value outside!
			xv := reflect.ValueOf(x)
			// send an error to stream if the flip not accept error
			if e, ok := x.(error); ok && !o.flip_accept_error {
				o.sendToFlow(ctx, e, out)
				continue
			}
			if ftop.opFunc(ctx, o, xv, out) {
				end = true
			}
		}
		if o.flip != nil {
			buffer := (reflect.ValueOf(o.flip))
			if buffer.Kind() != reflect.Slice {
				panic("flip is not buffer")
			}
			for i := 0; i < buffer.Len(); i++ {
				o.sendToFlow(ctx, buffer.Index(i).Interface(), out)
			}
		}

		o.closeFlow(out)
	}()
}
```

原来对于**threading**的一系列操作~~大抵可以先去掉了。~~，值得注意的是，后续会有**Last/TakeLast**操作，需要一个缓存来实现，这里就直接通过**Observable**里面的**flip**来完成。

```go
func (parent *Observable) Debounce(timespan time.Duration) (o *Observable) {
	o = parent.newFilterObservable("debounce")
	o.flip_accept_error = true
	o.flip_sup_ctx = true
	count := 0
	o.operator = filteringOperator{
		opFunc: func(ctx context.Context, o *Observable, item reflect.Value, out chan interface{}) (end bool) {
			count++
			go func() {
				tempCount := count
				time.Sleep(timespan)
				select {
				case <-ctx.Done():
					return
				default:
					if tempCount == count {
						o.sendToFlow(ctx, item.Interface(), out)
					}
				}
			}()
			return false
		},
	}
	return o
}
```

**Debounce**的功能就是消除抖动，在这里，我就简单地用一个go程+一个延时函数来实现，若是过了这一段时间还没有新的item到来(在这里表现为count并未改变)，再把之前的那个item输出出去。

```go
func (parent *Observable) Distinct() (o *Observable) {
	o = parent.newFilterObservable("distinct")
	o.flip_accept_error = true
	o.flip_sup_ctx = true
	m := map[string]bool{}
	o.operator = filteringOperator{
		opFunc: func(ctx context.Context, o *Observable, item reflect.Value, out chan interface{}) (end bool) {
			itemStr := fmt.Sprintf("%v", item)
			if _, ok := m[itemStr]; !ok {
				m[itemStr] = true
				o.sendToFlow(ctx, item.Interface(), out)
			}
			return false
		},
	}
	return o
}
```

**Distinct**就是将重复的数据过滤掉，这里我用一个map来判断当前数据是否是之前就出现过了。通过**fmt.Sprintf**来将**item**转化为相应的String，或许也可以通过``json``来完成，[上一次作业](https://blog.csdn.net/qq_43278234/article/details/109278731)实现的对象序列化也可以派上用场了。

```go
func (parent *Observable) ElementAt(num int) (o *Observable) {
	o = parent.newFilterObservable("elementAt.n")
	o.flip_accept_error = true
	o.flip_sup_ctx = true
	count := 0
	o.operator = filteringOperator{
		opFunc: func(ctx context.Context, o *Observable, item reflect.Value, out chan interface{}) (end bool) {
			if count == num {
				o.sendToFlow(ctx, item.Interface(), out)
				return true
			}
			count++
			return false
		},
	}

	return o
}
```

**ElementAt**直接就是发送对应第n项的数据，因此直接用一个count来计数就可以了。

```go
func (parent *Observable) First() (o *Observable) {
	o = parent.newFilterObservable("first")
	o.flip_accept_error = true
	o.flip_sup_ctx = true
	o.operator = filteringOperator{
		opFunc: func(ctx context.Context, o *Observable, item reflect.Value, out chan interface{}) (end bool) {
			o.sendToFlow(ctx, item.Interface(), out)
			return true
		},
	}
	return o
}
```

**First**的功能是发射第一项数据，因而直接发送第一项就行了。

```go
func (parent *Observable) IgnoreElements() (o *Observable) {
	o = parent.newFilterObservable("ignoreElements")
	o.flip_accept_error = true
	o.flip_sup_ctx = true
	o.operator = filteringOperator{
		opFunc: func(ctx context.Context, o *Observable, item reflect.Value, out chan interface{}) (end bool) {
			return false
		},
	}
	return o
}
```

**IgnoreElements**不发射任何数据，只发射Observable的终止通知，可以说这一个是最简单的了，因为几乎什么事都不用干。

```go
func (parent *Observable) Last() (o *Observable) {
	o = parent.newFilterObservable("last")
	o.flip_accept_error = true
	o.flip_sup_ctx = true
	o.operator = filteringOperator{
		opFunc: func(ctx context.Context, o *Observable, item reflect.Value, out chan interface{}) (end bool) {
			o.flip = append([]interface{}{}, item.Interface())
			return false
		},
	}
	return o
}
```

**Last**是要发送最后一项数据，前面说的flip就派上用场了，把它当作一个缓存来使用，实现一定的记忆化。这是因为在监听数据的时候，我们并不知道哪一个是真正的“最后一个”。

```go
func (parent *Observable) Sample(sample chan interface{}) (o *Observable) {
	o = parent.newFilterObservable("sample")
	o.flip_accept_error = true
	o.flip_sup_ctx = true
	var latest interface{} = nil
	o.operator = filteringOperator{
		opFunc: func(ctx context.Context, o *Observable, item reflect.Value, out chan interface{}) (end bool) {
			latest = item.Interface()
			go func() {
				tempEnd := true
				for tempEnd {
					select {
					case <-ctx.Done():
						tempEnd = true
					case <-sample:
						if latest != nil {
							if o.sendToFlow(ctx, latest, out) {
								tempEnd = false
							}
							latest = nil
						}
					}
				}
			}()
			return false
		},
	}
	return o
}
```

**Sample**就是定期发射Observable最近发射的数据项，在这里我的实现思路就是持续监听管道**Sample**，一旦监听到消息就把当前最新的数据项发送出去，~~思路倒是这样没错，只是我的这个没错，只是这个实现方式倒是感觉有点怪怪的，只不过还没想到其他高端一点的实现~~

```go
func (parent *Observable) Skip(num int) (o *Observable) {
	o = parent.newFilterObservable("skip.n")
	o.flip_accept_error = true
	o.flip_sup_ctx = true
	count := 0
	o.operator = filteringOperator{
		opFunc: func(ctx context.Context, o *Observable, item reflect.Value, out chan interface{}) (end bool) {
			count++
			if count > num {
				o.sendToFlow(ctx, item.Interface(), out)
			}
			return false
		},
	}

	return o
}
```

**Skip**就是跳过前n个，也就是说用一个count来计数，计到n个之后才发送。

```go
func (parent *Observable) SkipLast(num int) (o *Observable) {
	o = parent.newFilterObservable("skipLast.n")
	o.flip_accept_error = true
	o.flip_sup_ctx = true
	count := 0
	var lasts []interface{}
	o.operator = filteringOperator{
		opFunc: func(ctx context.Context, o *Observable, item reflect.Value, out chan interface{}) (end bool) {
			if count == num {
				o.sendToFlow(ctx, lasts[0], out)
				lasts = lasts[1:]
			} else {
				count++
			}
			lasts = append(lasts, item.Interface())
			return false
		},
	}

	return o
}
```

**SkipLast**要跳过后n个。

![在这里插入图片描述](https://img-blog.csdnimg.cn/2020110618471018.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

根据官方文档的实例来看，其实也就是往后延n个，因此在这里我自定义了一个缓存来实现延后n个。

```go
func (parent *Observable) Take(num int) (o *Observable) {
	o = parent.newFilterObservable("take.n")
	o.flip_accept_error = true
	o.flip_sup_ctx = true
	count := 0
	o.operator = filteringOperator{
		opFunc: func(ctx context.Context, o *Observable, item reflect.Value, out chan interface{}) (end bool) {
			count++
			if count > num {
				return true
			}
			o.sendToFlow(ctx, item.Interface(), out)
			return false
		},
	}

	return o
}
```

**Take**，取前面n项数据，同样也是用count来计数。

```go
func (parent *Observable) TakeLast(num int) (o *Observable) {
	o = parent.newFilterObservable("takeLast.n")
	o.flip_accept_error = true
	o.flip_sup_ctx = true
	count := 0
	var lasts []interface{}
	o.operator = filteringOperator{
		opFunc: func(ctx context.Context, o *Observable, item reflect.Value, out chan interface{}) (end bool) {
			count++
			if count <= num {
				lasts = append(lasts, item.Interface())
			} else {
				lasts = lasts[1:]
				lasts = append(lasts, item.Interface())
			}
			o.flip = lasts
			return false
		},
	}

	return o
}
```

**TakeLast**的话，跟前面的**Last**有些许类似，都是通过flip来实现了。

至此，filtering的各个功能都实现得差不多了，~~除了Debounce跟Sample自我感觉有点不够完善以外，其他的也都大抵让人满意。~~

# 测试

新建一个文件``filtering_test.go``，在里面测试上述的各项功能。

```go
package rxgo

import (
	"fmt"
	"time"
)

func ExampleDistinct() {
	Just(22, 12, 12, 13, 7, 5, 6, 22).Distinct().Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()
	//Output:221213756
}

func ExampleElementAt() {
	Just(2, 1, 12, 13, 17, 5, 6, 22).ElementAt(5).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()
	//Output:5
}

func ExampleFirst() {
	Just(23, 11, 2, 3, 1, 25, 66).First().Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()
	//Output:23
}

func ExampleIgnoreElements() {
	Just(3, 11, 12, 15, 2, 6).IgnoreElements().Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()
	//Output:
}

func ExampleLast() {
	Just(33, 1, 0, 215, 4, 6).Last().Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()
	//Output:6
}

func ExampleSample() {
	observableP := make(chan interface{})
	go func() {
		Just(1, 2, 3, 4, 5).Map(func(x int) int {
			switch x {
			case 1:
				time.Sleep(0 * time.Millisecond)
			case 2:
				time.Sleep(1 * time.Millisecond)
			case 3:
				time.Sleep(2 * time.Millisecond)
			case 4:
				time.Sleep(2 * time.Millisecond)
			default:
				time.Sleep(2 * time.Millisecond)
			}
			return x
		}).Subscribe(func(x int) {
			observableP <- x
		})
	}()
	Just(1, 2, 3, 4, 5).Map(func(x int) int {
		time.Sleep(2 * time.Millisecond)
		return x
	}).Sample(observableP).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()
	//Output:123
}

func ExampleSkip() {
	Just(3, 21, 0, 25, 24, 63, 77).Skip(2).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()
	//Output:025246377
}

func ExampleSkipLast() {
	Just(3, 21, 0, 25, 24, 63, 77).SkipLast(2).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()
	//Output:32102524
}

func ExampleTake() {
	Just(3, 21, 0, 25, 24, 63, 77).Take(4).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()
	//Output:321025
}

func ExampleTakeLast() {
	Just(3, 21, 0, 25, 24, 63, 77).TakeLast(4).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()
	//Output:25246377
}

func ExampleDebounce() {
	time.Sleep(250 * time.Millisecond)
	Just(0, 1, 2, 3, 4, 5).Map(func(x int) int {
		switch x {
		case 0:
			time.Sleep(0 * time.Millisecond)
		case 1:
			time.Sleep(1 * time.Millisecond)
		case 2:
			time.Sleep(3 * time.Millisecond)
		case 3:
			time.Sleep(3 * time.Millisecond)
		case 4:
			time.Sleep(1 * time.Millisecond)
		default:
			time.Sleep(1 * time.Millisecond)
		}
		return x
	}).Debounce(2 * time.Millisecond).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()
	//Output:12
}
```

执行指令``go test -v``，能够看到测试结果。

![在这里插入图片描述](https://img-blog.csdnimg.cn/2020110619024771.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

然后新建一个文件``main.go``，通过``import RxGo "github.com/pmlpml/rxgo"``来导入rxgo包，再去~~随便~~测试一下刚刚实现的功能。

```go
package main

import (
	"fmt"
	"time"
	RxGo "github.com/pmlpml/rxgo"
)

func main() {
	RxGo.Just(22, 12, 12, 13, 7, 5, 6, 22).Distinct().Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(2, 1, 12, 13, 17, 5, 6, 22).ElementAt(5).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(23, 11, 2, 3, 1, 25, 66).First().Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(3, 11, 12, 15, 2, 6).IgnoreElements().Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(33, 1, 0, 215, 4, 6).Last().Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(3, 21, 0, 25, 24, 63, 77).Skip(2).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(3, 21, 0, 25, 24, 63, 77).SkipLast(2).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(3, 21, 0, 25, 24, 63, 77).Take(4).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()
	
	RxGo.Just(3, 21, 0, 25, 24, 63, 77).TakeLast(4).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	observableP := make(chan interface{})
	go func() {
		RxGo.Just(1, 2, 3, 4, 5).Map(func(x int) int {
			switch x {
			case 1:
				time.Sleep(3 * time.Millisecond)
			case 2:
				time.Sleep(1 * time.Millisecond)
			case 3:
				time.Sleep(2 * time.Millisecond)
			case 4:
				time.Sleep(2 * time.Millisecond)
			default:
				time.Sleep(1 * time.Millisecond)
			}
			return x
		}).Subscribe(func(x int) {
			observableP <- x
		})
	}()
	RxGo.Just(1, 2, 3, 4, 5).Map(func(x int) int {
		time.Sleep(2*time.Millisecond)
		return x
	}).Sample(observableP).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(0, 1, 1, 3, 1, 5).Map(func(x int) int {
		switch x {
		case 0:
			time.Sleep(0 * time.Millisecond)
		case 1:
			time.Sleep(1 * time.Millisecond)
		case 3:
			time.Sleep(2 * time.Millisecond)
		case 5:
			time.Sleep(3 * time.Millisecond)
		default:
			time.Sleep(1 * time.Millisecond)
		}
		return x
	}).Debounce(2 * time.Millisecond).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

}
```

执行``go run main.go``可以看到结果。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201106190706225.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)
