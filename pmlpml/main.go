package main

import (
	"fmt"
	"time"
	RxGo "github.com/pmlpml/rxgo"
)

func main() {
	RxGo.Just(22, 12, 12, 13, 7, 5, 6, 22).Distinct().Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(2, 1, 12, 13, 17, 5, 6, 22).ElementAt(5).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(23, 11, 2, 3, 1, 25, 66).First().Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(3, 11, 12, 15, 2, 6).IgnoreElements().Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(33, 1, 0, 215, 4, 6).Last().Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(3, 21, 0, 25, 24, 63, 77).Skip(2).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(3, 21, 0, 25, 24, 63, 77).SkipLast(2).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(3, 21, 0, 25, 24, 63, 77).Take(4).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()
	
	RxGo.Just(3, 21, 0, 25, 24, 63, 77).TakeLast(4).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	observableP := make(chan interface{})
	go func() {
		RxGo.Just(1, 2, 3, 4, 5).Map(func(x int) int {
			switch x {
			case 1:
				time.Sleep(3 * time.Millisecond)
			case 2:
				time.Sleep(1 * time.Millisecond)
			case 3:
				time.Sleep(2 * time.Millisecond)
			case 4:
				time.Sleep(2 * time.Millisecond)
			default:
				time.Sleep(1 * time.Millisecond)
			}
			return x
		}).Subscribe(func(x int) {
			observableP <- x
		})
	}()
	RxGo.Just(1, 2, 3, 4, 5).Map(func(x int) int {
		time.Sleep(2*time.Millisecond)
		return x
	}).Sample(observableP).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

	RxGo.Just(0, 1, 1, 3, 1, 5).Map(func(x int) int {
		switch x {
		case 0:
			time.Sleep(0 * time.Millisecond)
		case 1:
			time.Sleep(1 * time.Millisecond)
		case 3:
			time.Sleep(2 * time.Millisecond)
		case 5:
			time.Sleep(3 * time.Millisecond)
		default:
			time.Sleep(1 * time.Millisecond)
		}
		return x
	}).Debounce(2 * time.Millisecond).Subscribe(func(x int) {
		fmt.Print(x)
	})
	fmt.Println()

}
