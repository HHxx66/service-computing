## 开发 web 服务程序基本要求的实现

根据课件[HTTP协议 与 web/http 库使用指南](https://pmlpml.gitee.io/service-computing/post/service-1-http-and-go-server/#43-%E7%90%86%E6%80%A7%E7%9C%8B%E5%BE%85%E5%90%84%E7%A7%8D-web-%E5%BC%80%E5%8F%91%E6%A1%86%E6%9E%B6)，直接可以仿照得到``main.go``和``service/server.go``。

``main.go``

```go
package main

import (
	"os"

	"github.com/github-user/cloudgo/service"
	flag "github.com/spf13/pflag"
)

const (
	PORT string = "8080"
)

func main() {
	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = PORT
	}

	pPort := flag.StringP("port", "p", PORT, "PORT for httpd listening")
	flag.Parse()

	if len(*pPort) != 0 {
		port = *pPort
	}

	server := service.NewServer()
	server.Run(":" + port)
}
```

``service/server.go``

```go
package service

import (
	"net/http"
	"os"

	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"github.com/unrolled/render"
)

// NewServer configures and returns a Server.
func NewServer() *negroni.Negroni {

	formatter := render.New(render.Options{
		Directory:  "templates",
		Extensions: []string{".html"},
		IndentJSON: true,
	})

	n := negroni.Classic()
	mx := mux.NewRouter()

	initRoutes(mx, formatter)

	n.UseHandler(mx)
	return n
}

func initRoutes(mx *mux.Router, formatter *render.Render) {
	webRoot := os.Getenv("WEBROOT")
	if len(webRoot) == 0 {
		if root, err := os.Getwd(); err != nil {
			panic("Could not retrive working directory")
		} else {
			webRoot = root
			//fmt.Println(root)
		}
	}

	mx.HandleFunc("/api/test", apiTestHandler(formatter)).Methods("GET")

    mx.PathPrefix("/static").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(webRoot+"/assets/"))))

	mx.HandleFunc("/", homeHandler(formatter)).Methods("GET")

	mx.HandleFunc("/login", loginHandler(formatter)).Methods("GET")

	mx.HandleFunc("/login", tableform).Methods("POST")

}
```

此处把Handler函数分离到了``service/handlers.go``。

再根据下一个课件[golang web 服务器端编程](https://pmlpml.gitee.io/service-computing/post/service-2-go-server-programming/)，完善``service/handlers.go``里面的各个handler函数。

```go
package service

import (
	"html/template"
	"net/http"

	"github.com/unrolled/render"
)

func apiTestHandler(formatter *render.Render) http.HandlerFunc {

	return func(w http.ResponseWriter, req *http.Request) {
		formatter.JSON(w, http.StatusOK, struct {
			ID      string `json:"id"`
			Content string `json:"content"`
		}{ID: "8675309", Content: "Hello from Go!"})
	}
}

func homeHandler(formatter *render.Render) http.HandlerFunc {

	return func(w http.ResponseWriter, req *http.Request) {
		formatter.HTML(w, http.StatusOK, "index", struct {}{})
	}
}

func loginHandler(formatter *render.Render) http.HandlerFunc {

	return func(w http.ResponseWriter, req *http.Request) {
		formatter.HTML(w, http.StatusOK, "login", struct {}{})
	}
}

func tableform(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	username := template.HTMLEscapeString(r.Form.Get("username"))
	password := template.HTMLEscapeString(r.Form.Get("password"))
	t := template.Must(template.New("detail.html").ParseFiles("./templates/detail.html"))
	err := t.Execute(w, struct {
		Username string
		Password string
	}{Username: username, Password: password})
	if err != nil {
		panic(err)
	}
}

```

不过，要想程序能够运行，还需要执行三条指令。

``go get github.com/codegangsta/negroni``

``go get github.com/gorilla/mux``

``go get github.com/unrolled/render``

### 支持静态文件服务

```go
mx.PathPrefix("/static").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(webRoot+"/assets/"))))
```

访问http://localhost:8080/static/js/hello.js得到

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201122193607569.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

访问http://localhost:8080/static/css/main.css得到

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201122193715584.png#pic_center)

### 支持简单 js 访问支持简单 js 访问

```go
func apiTestHandler(formatter *render.Render) http.HandlerFunc {

	return func(w http.ResponseWriter, req *http.Request) {
		formatter.JSON(w, http.StatusOK, struct {
			ID      string `json:"id"`
			Content string `json:"content"`
		}{ID: "8675309", Content: "Hello from Go!"})
	}
}
```

结果就是获得一个ID跟Content。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201122194715287.png#pic_center)

同样地主页也可以通过``hello.js``来获取ID跟Content。

``hello.js``

```js
$(document).ready(function() {
    $.ajax({
        url: "/api/test"
    }).then(function(data) {
       $('.greeting-id').append(data.id);
       $('.greeting-content').append(data.content);
    });
});
```

```go
func homeHandler(formatter *render.Render) http.HandlerFunc {

	return func(w http.ResponseWriter, req *http.Request) {
		formatter.HTML(w, http.StatusOK, "index", struct {}{})
	}
}
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201122194938981.png#pic_center)

### 提交表单，并输出一个表格（必须使用模板）

可以参考文档[4.1 处理表单的输入](https://astaxie.gitbooks.io/build-web-application-with-golang/content/zh/04.1.html)。

**模板**

``login.html``

```html
<html>
<head>
   <link rel="stylesheet" href="static/css/main.css"/>
</head>
<body>
<form action="/login" method="post">
    用户名:<input type="text" name="username">
    密码:<input type="text" name="password">
    <input type="submit" value="登录">
</form>
</body>
</html>
```

``detail.html``

```html
<html>
    
<head>
    <link rel="stylesheet" href="static/css/main.css"/>
</head>

<body>

    <table>
        <tr>
            <td>Username</td>
            <td>Password</td>
        </tr>
        <tr>
            <td>{{.Username}}</td>
            <td>{{.Password}}</td>
        </tr>
    </table>
</body>

</html>
```

```go
mx.HandleFunc("/login", loginHandler(formatter)).Methods("GET")

mx.HandleFunc("/login", tableform).Methods("POST")
```

```go
func loginHandler(formatter *render.Render) http.HandlerFunc {

	return func(w http.ResponseWriter, req *http.Request) {
		formatter.HTML(w, http.StatusOK, "login", struct {}{})
	}
}

func tableform(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	username := template.HTMLEscapeString(r.Form.Get("username"))
	password := template.HTMLEscapeString(r.Form.Get("password"))
	t := template.Must(template.New("detail.html").ParseFiles("./templates/detail.html"))
	err := t.Execute(w, struct {
		Username string
		Password string
	}{Username: username, Password: password})
	if err != nil {
		panic(err)
	}
}
```

提交前：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201122200352896.png#pic_center)

提交后：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201122200405221.png#pic_center)

### 使用 curl 测试

``curl -v http://localhost:8080``

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201122200706838.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

``curl -v http://localhost:8080/login``

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201122200945823.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

### 使用 ab 测试

首先需要安装 Apache web 压力测试程序。

``sudo yum -y install httpd-tools --nogpgcheck``

根据[CentOS服务器Http压力测试之ab](http://linux.it.net.cn/CentOS/fast/2015/0715/16393.html)。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201122201329608.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

**ab性能指标**：

```shell
Document Path:          /  ###请求的资源
Document Length:        50679 bytes  ###文档返回的长度，不包括相应头


Concurrency Level:      3000   ###并发个数
Time taken for tests:   30.449 seconds   ###总请求时间
Complete requests:      3000     ###总请求数
Failed requests:        0     ###失败的请求数
Write errors:           0
Total transferred:      152745000 bytes
HTML transferred:       152037000 bytes
Requests per second:    98.52 [#/sec] (mean)      ###平均每秒的请求数
Time per request:       30449.217 [ms] (mean)     ###平均每个请求消耗的时间
Time per request:       10.150 [ms] (mean, across all concurrent requests)  ###上面的请求除以并发数
Transfer rate:          4898.81 [Kbytes/sec] received   ###传输速率


Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        2   54  27.1     55      98
Processing:    51 8452 5196.8   7748   30361
Waiting:       50 6539 5432.8   6451   30064
Total:         54 8506 5210.5   7778   30436


Percentage of the requests served within a certain time (ms)
  50%   7778   ###50%的请求都在7778Ms内完成
  66%  11059
  75%  11888
  80%  12207
  90%  13806
  95%  18520
  98%  24232
  99%  24559
 100%  30436 (longest request)
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201122201822171.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)


那么通过指令``ab -n 1000 -c 100 http://localhost:8080/``则有

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201122201642967.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201122201657475.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)
