# 程序包开发，读简单配置文件v1的代码使用方式

首先在虚拟机配置好go语言开发环境，具体可参照我的第一次作业[服务计算(1)——安装 go 语言开发环境](https://blog.csdn.net/qq_43278234/article/details/108566781)

## 安装golang

``sudo yum install golang --nogpgcheck``

## 设置环境变量：

通过vi编辑~/.profile文件，这个文件不存在（即显示为新文件）也没有关系。在文件中添加：

``export GOPATH=$HOME/gowork``

``export PATH=$PATH:$GOPATH/bin``

最后执行这些配置

``source $HOME/.profile``

## 程序运行

将代码文件放入路径$GOPATH/src/github.com/github-user/。

在目录github.com/github-user/test/readini下执行``go test -v``

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201018160536315.png#pic_center)

在目录github.com/github-user/test/main下执行``go run main.go``

![在这里插入图片描述](https://img-blog.csdnimg.cn/2020101816125675.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjc4MjM0,size_16,color_FFFFFF,t_70#pic_center)