package readini

import "fmt"
import "os"
import "time"
import "bufio"

type ListenFunc func(string)

func (l ListenFunc) Listen(inifile string) {
	l(string(inifile))
}

func ExampleInit() {
	Init()
	fmt.Printf("%c\n",CommentSymbol)
	//Output:#
}

func ExampleWatch() {
	var lis ListenFunc = func(inifile string) {
		before_info, err := os.Lstat(inifile)
		if err != nil {
			panic(err)
		}
		for {
			after_info, err := os.Lstat(inifile)
			if err != nil {
				panic(err)
			}
			if !before_info.ModTime().Equal(after_info.ModTime()) {
				break
			}
			time.Sleep(time.Duration(1)*time.Second)
		}
	}
	go func(){
		conf, err := Watch("example.ini", lis)
		if err != nil {
			fmt.Println(err)
		}
		for s, _ := range conf {
			fmt.Println("Section: ", s)
			for _, value := range conf[s] {
				for k, v := range value {
					fmt.Println("Key:", k, "\tValue:", v)
				}
			}
			fmt.Println()
		}
	}()
	file,openErr:=os.Create("example.ini")
	defer os.Remove("example.ini")
	defer file.Close()
	if openErr!=nil {
		panic(openErr)
	}
	time.Sleep(time.Duration(1)*time.Second)
	writer:=bufio.NewWriter(file)
	_,errWrite := writer.Write([]byte("[test]\n"))
	_,errWrite = writer.Write([]byte("value1 = 123\n"))
	_,errWrite = writer.Write([]byte("value2 = 222\n"))
	if errWrite!=nil {
		os.Exit(0)
	}
	writer.Flush()
	time.Sleep(time.Duration(2)*time.Second)
/*Output: 
Section:  test
Key: value1 	Value: 123
Key: value2 	Value: 222
*/
}
