package readini

import (
	"runtime"
	"bufio"
	"errors"
	"io"
	"os"
	"strings"
)

// Section下面的键值对
type Element map[string]string

// ini文件结构(对象)
// Object为各个Section所对应的所有键值对
type Configuration map[string][]Element

// Listener接口
type Listener interface {
	Listen(inifile string)
}

// 当前系统下的注释符
var CommentSymbol byte

// 通过确定当前操作系统，从而确定相应的注释符
func Init() {
	sysType := runtime.GOOS
	if sysType == "linux" {
		CommentSymbol = '#'
	} 
 	if sysType == "windows" {
		CommentSymbol = ';'
	}
}

// 监听自函数运行以来发生的一次配置文件变化并返回最新的配置文件解析内容。
func Watch(filename string, listener Listener) (Configuration, error) {
	listener.Listen(filename)
	i := make(Configuration)
	var e error = nil
	f, err := os.Open(filename)
	if err != nil {
		e = errors.New("Open file faild.")
		return i, e
	}
	defer f.Close()
	// 将ini文件转换成一个bufio
	r := bufio.NewReader(f)
	// 当前所解析到的section
	section := ""
	for {
		// 以'\n'作为结束符读入一行
		line, err := r.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			e = errors.New("Read file faild.")
			break
		}
		// 删除行两端的空白字符
		line = strings.TrimSpace(line)
		// 解析一行中的内容
		
		// 空行则跳过
		if line == "" {
			continue
		}
		// 以符号CommentSymbol作为注释
		if line[0] == CommentSymbol {
			continue
		}
		length := len(line)
		// 匹配字符串
		if line[0] == '[' && line[length-1] == ']' { // section
			section = line[1 : length-1]
			// 如果map中没有这个section，添加进来
			if _, ok := i[section]; !ok {
				i[section] = []Element{}
			}
		}else { // 键值对数据
			// 分割字符串
			s := strings.Split(line, "=")
			if len(s) < 2 {
				e = errors.New("Incorrect key-value pair format.")
				break
			}
			key := strings.TrimSpace(s[0])
			value := strings.TrimSpace(s[1])
			element := make(Element)
			element[key] = value
			// 把键值对添加进section里面
			if section == "" {
				i[section] = []Element{}
			}
			if _, ok := i[section]; ok {
				i[section] = append(i[section], element)
			}
		}
	}
	return i, e
}
